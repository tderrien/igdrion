So far, 2 tools have been tested
 2 tools :
 - **NanoSim**           : 
     - https://github.com/bcgsc/NanoSim
     - `/groups/dog/script/NanoSim/test.sh`
 - **DeepSimulator** : 
   - https://github.com/liyu95/DeepSimulator
   - `/groups/dog/script/DeepSimulator_gpu/DeepSimulator/README_THOMAS`

Could probably be interesting to relaunch Fabien's snakemake pipeline with known/simulated reads to assess performance of tools. 

