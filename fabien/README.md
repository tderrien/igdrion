# Transcriptomics flowchart

Flowchart for the evaluation of tools dedicated to transcriptome reconstruction based on LR-RNASeq.

```mermaid
    graph TD;
    subgraph FAST5_PROCESSING;
    run1.fast5-->|guppy_hac?|run1_hac.fastq;
    run1_hac.fastq-->|correction?|run1_hac_cor.fastq;
    run1.fast5-->run1.fastq;
    end

    subgraph MAPPING;
    run1.fastq-->|minimap2 wrt REFannot|run1.bam;
    run1_hac_cor.fastq-->|minimap2 wrt REFannot|run1.bam;
    end;

    subgraph TX_RECONSTRUCTION;
    run1.bam --> ToolX & Flair & Talon & Stringtie & Bambu--> ToolX.gtf ;
    ToolX.gtf -->|filter exon|ToolX_ex.gtf;
    end;

    subgraph FILTER AND EVALUATION;
    ToolX_ex.gtf -->|bedtools intersect reads.bam|ToolX_ex_val.gtf;
    ToolX_ex_val.gtf-->|gffcompare|Tool_ex_val.stats;
    Tool_ex_val.stats-->|parse|Summary.tsv;
    Summary.tsv-->|ggplot|Performance_metrics;
    end;
```
