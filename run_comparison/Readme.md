# IGDRion : run comparison with [NanoComp](https://github.com/wdecoster/NanoComp)

This directory summarizes how to run QC with NanoComp in order to compare MinION reports done from IGDR.
All run informations should be stored in the `all_summary.txt` file.

# Install nanocomp

```
pip3 install NanoComp
```

# Run on Nanocomp

```
# List sequencing_summary.txt files from RackStation IGDR
find  /Volumes/Chiens/NanoporeDATA/  -name "*_summary*.txt"  | grep -v "final"

# launch the run_nanocomp.sh depending on the summary file
bash run_nanocomp.sh ../all_summary.txt
 
``` 

#First runs : Jan 2020 
NanoComp report should be present at [NanoComp-report-2020-03-03.html](http://tools.genouest.org/data/tderrien/igdrion/NanoComp-report-2020-03-03.html).   
#4 canine cDNA runs : June 2020
NanoComp report should be present at [NanoComp-report-2020-07-24.html](http://tools.genouest.org/data/tderrien/igdrion/NanoComp-report-2020-07-24.html).


