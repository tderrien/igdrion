#!/bin/bash

# Run Nanocomp using the config summaryfile.txt  of different runs
###############################################@

usage() {
    echo "#" >&2
    echo -e "# USAGE: `basename $0` summary_file.txt ">&2
}


# Check infile
if [ ! -r "$1" ];then

    echo "# Ooops...">&2
    echo "# Needs an input summary  file" >&2
    usage;
    exit 1;
else
    infile=$1;
fi



LISTFILE=$(awk '$1!~/^#/{print $NF}'          $infile | transp.awk)
LISTNAME=$(awk '$1!~/^#/{print $2"-"$3"-"$4}' $infile | transp.awk)
LISTCOL=$( awk '$1!~/^#/{print $5}'           $infile | transp.awk)
NBRUN=$(awk '$1!~/^#/{print NR}' 	      $infile | tail -1)

# colors        : https://matplotlib.org/3.1.0/gallery/color/named_colors.html
DATECUR=$(date +%F)
OUTDIR=./compare-runs-${NBRUN}-${DATECUR}
date
echo "Run:
  NanoComp -t 10 --summary $LISTFILE --colors $LISTCOL --outdir $OUTDIR --names $LISTNAME"
time NanoComp -t 10 --summary $LISTFILE --colors $LISTCOL --outdir $OUTDIR --names $LISTNAME

# add to genouest for visualisation
DIR_GENO="/home/genouest/umr6061/recomgen/tderrien/nano_report/"
scp $OUTDIR/NanoComp-report.html tderrien@genossh.genouest.org:/home/genouest/cnrs_umr6290/tderrien/webdata/igdrion/NanoComp-report-${DATECUR}.html

echo "Check file:
http://tools.genouest.org/data/tderrien/igdrion/NanoComp-report-${DATECUR}.html"

