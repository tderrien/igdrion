#!/bin/bash

# Guppy version 
VERSION=4.2.2

#SBATCH --gres=gpu:2 -p gpu
#SBATCH --output=basecall_ont_gpu_scratch_guppy.txt

# input dir
FAST5DIR=/groups/dog/stage/manon/Data/MetaGRun/MinION/fast5_pass/

# new guppy version
BINDIR=/groups/dog/script/ont/ont-guppy/bin/
SAVED=/scratch/tderrien/fastq_v2_gpu_guppy${VERSION}/
cfg=$BINDIR/data/dna_r9.4.1_450bps_hac.cfg


time $BINDIR/guppy_basecaller --input_path $FAST5DIR --save_path $SAVED -x "cuda:0 cuda:1" --config $cfg 

# Then mv from scratch to current dir
mv $SAVED/ .

# check top with in gpu with 
# watch nvidia-smi
# Pour lancer dans le terminal : sbatch basecall_ont.sh

# Eventually, mv basecall_ont_gpu_scratch.txt basecall_ont_gpu_scratch_${VERSION}.txt
