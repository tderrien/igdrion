# Launch Test 

```
sbatch basecall_ont.sh
```

# Test of guppy versions

Storage of ONT guppy versions are localized here: 

```
/groups/dog/script/ont/
```
*Warnings*: Each time the archive is decompressed, it overwrites the old one

# Data test 

Metagenomic run (LSK) with `FAST5_pass` directory of **286Go**
```
du -sh /groups/dog/stage/manon/Data/MetaGRun/MinION/fast5_pass/
286G	/groups/dog/stage/manon/Data/MetaGRun/MinION/fast5_pass/
```
*Warnings*: Would be interesting to test on RAD004 run
