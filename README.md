
**[Deprecated]** : Informations can be accessed following these 2 dedicated webpages:
 - public repos for tools developed : https://github.com/IGDRion
 - private repos for sequencing data information : https://gitlab.com/bioinfog/nanopore/projects
---
---


# IGDRion : MinION@IGDR

This project aims at summarizing informations regarding the process of using [MinION sequencing](https://nanoporetech.com/) technology at [IGDR](https://igdr.univ-rennes1.fr).


It can be used for many applications including (not exhaustively):
 * ***Basic Genome Research***
   * Resolve challenging regions (repeats) given ultra long reads.
   * Identify RNA/DNA chemical modifications (methylation, acetylation) via direct sequencing.
 * ***RNA Research***
   * Characterise and quantify full-length transcripts using long reads (direct exon connectivity)
   * Explore epigenetic modifications through direct RNA sequencing.
 * ***Clinical Research***
    * Map complex Structural Variants (SVs).
    * Phase variants using long reads.
 * ***Microbiome***
   * Assemble complete genomes.
   * Sequence at sample source (given portability).

For any specific informations, please see [Contacts](https://gitlab.com/tderrien/igdrion/-/blob/master/README.md#contacts).

For details about protocols, see wikis **[biological](wikis/biological_protocol.md)** or **[bioinformatic](wikis/bioinformatic_protocol.md) protocols**.

## Localization 
The minION is localized Villejean Campus, Building 4,  **room 221** (2nd floor).


## Processing
```mermaid
    graph LR;
      Sample_preparation --> Library_construction;
      Library_construction --> Sequencing;
      Sequencing --> Bioinformatic_analyses;
     
```


## Purchase order

Connect to the nanopore store service : [Store](https://store.nanoporetech.com/)


## System
The minION is accompanied with the [Min-IT hardware solution](https://nanoporetech.com/products/minit).
It is composed of 256 core GPU, 8GB RAM and 512 GB of disk space.

## Bioinfog gitlab

Cross-project analyses are now available through the private gitlab group : https://gitlab.com/bioinfog/nanopore


## Contacts 

MinION user list @ IGDR :  igdrion@listes.univ-rennes1.fr

and/or 

* Edouard CADIEU : edouard.cadieu@univ-rennes1.fr
* Yann AUDIC : yann.audic@univ-rennes1.fr
* Thomas DERRIEN : thomas.derrien@univ-rennes1.fr
