## Command lines 



### Manipulate fastq/bam
Theses are (hopefully) useful command-lines for mapping fastq files and manipulating .bam/.sam files. Most of the tools are already installed on genocluester.   

To map DNA ONT .fq reads with [minimap2](https://github.com/lh3/minimap2):   
```shell
minimap2 -ax map-ont ref.fa ont-reads.fq  > aln.sam
```
You can pipe into .bam with `| samtools view -bS - > file.bam`.
You can also use `--secondary=no` to suppress secondary alignments (aka multiple mappings), but you can't suppress supplementary alignment (aka split or chimeric alignment) this way. You can use samtools to filter out these alignments:
```
minimap2 -ax map-out ref.fa reads.fq | samtools view -F0x900
```
   


To extract unmapped reads in .bam file using [samtools](http://www.htslib.org):   
```console
samtools view -b -f 4 file.bam > unmapped.bam
```


To convert .bam into .fastq using [bedtools](https://bedtools.readthedocs.io/en/latest/) :    
```bash
bedtools bamtofastq [OPTIONS] -i <BAM> -fq <FASTQ>
```
