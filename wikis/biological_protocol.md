This pages summarises informations/tips regarding DNA extraction from cell culture.
For a comparison of all protocols, use the dedicated [nanopore web page](https://community.nanoporetech.com/protocols) (validity 8/3/2020).

## Extraction

See [Nanopore page](https://community.nanoporetech.com/extraction_methods) for full cases.

## DNA
It is largely inspired by Edouard's comments :-) and recently published (Feb 2020) protocol for [HMW gDNA purification and ONT ultra-long-read data generation](https://www.protocols.io/view/hmw-gdna-purification-and-ont-ultra-long-read-data-bchhit36) yielding >2Mb reads.

```mermaid
    graph LR;
      Cell_Culture --> Cell_lysis;
      Cell_lysis --> Phenol_chloroform_extraction;
      Phenol_chloroform_extraction  --> Ethanol_precipitation;
      Ethanol_precipitation  --> ONT_library_preparation;
     
```


## RNA

For [direct cDNA sequencing](https://nanoporetech.com/sites/default/files/s3/literature/Nanopore-cDNA-Guide.pdf), 4 steps from cells extract : 
1. Pellet 5.10^7 cells
2. Extract Total RNAs (Trizol or Nucleospin)
   * *Verification BioA RNA 6000 Nano Kit*
   * *Nanodrop + Qubit for QUality and Quantity*

`5 microg`

3. Select PolyA+ RNAs (NEB NExt polyA magnetic isolation)
   * *Verification BioA RNA picochip*

`100 nanog polyA`

4. Direct cDNA Library Preparation
   * *Qubit™ dsDNA HS Assay Kit (Q32851)*

`60 nanog polyA`

```mermaid
    graph LR;
      Cell_pellet --> Trizol_Chloroform;
      Cell_pellet --> Kit_Macherey(Nucleospin_column);
      Trizol_Chloroform  --> polyA_Selection;
      Kit_Macherey(Nucleospin_column) --> polyA_Selection;
      polyA_Selection --> Libray_Kit_Nanopore;
     
```

## Pores 

To get informations on [Oxford Nanopore](https://nanoporetech.com/how-it-works/types-of-nanopores).
