Tools ans pipeline dedicated to the analysis of metag/metat short and long read data.

A list of bioinformatic tools is available on this [website](https://long-read-tools.org/index.html) from the GB paper *[Opportunities and challenges in long-read sequencing data analysis](https://genomebiology.biomedcentral.com/track/pdf/10.1186/s13059-020-1935-5)*.

# Transcriptomic

There is an ongoing nf-core project dedicated to transcriptomic analysis: [Nanoseq](https://github.com/nf-core/nanoseq)   

Analyses involve (at least) 5 parts:
 - Basecalling (guppy hac?)
 - QC (Quality Control) : 
   - [NanoComp](https://github.com/wdecoster/NanoComp) 
   - [pycoQC](https://github.com/a-slide/pycoQC)
 - Mapping
   - [Minimap2](https://github.com/lh3/minimap2)
 - Transcriptome reconstruction/Quantification
   - [FLAIR](https://github.com/BrooksLabUCSC/flair)
   - [TALON](https://github.com/mortazavilab/TALON) 
   - [stringtie2 (long read)](https://github.com/gpertea/stringtie)
   - [Bambu](https://github.com/GoekeLab/bambu) and also data from Goeke lab via the [SG-NEx ; The Singapore Nanopore-Expression Project](https://github.com/Goekelab/sg-nex-data)
   - [isONclust- ont-denovo](https://github.com/nanoporetech/pipeline-nanopore-denovo-isoforms)
 - GFF/GTF comparison/visualization 
   - [GffCompare](https://f1000research.com/articles/9-304/v1) from Stringtie Auhtors
   - [Swan](https://github.com/mortazavilab/swan_vis) from TALON authors

# Metagenomic

This  would involve (at least) 5 parts:
 - QC (Quality Control) : 
   - [NanoComp](https://github.com/wdecoster/NanoComp) 
   - [pycoQC](https://github.com/a-slide/pycoQC)
 - read simulation:
   - ~~[ART](https://www.niehs.nih.gov/research/resources/software/biostatistics/art/)~~(only short reads and not maintained)
   - [NanoSim](https://github.com/bcgsc/NanoSim)
 - Correction : 
   - [Canu](https://github.com/marbl/canu)
 - Taxonomic Classification:
   - [MetaMaps](https://github.com/DiltheyLab/MetaMaps)
   - [Kraken2](https://ccb.jhu.edu/software/kraken2/)
   - [Centrifuge](https://ccb.jhu.edu/software/centrifuge/)
 - Assembly :
   - [Canu](https://github.com/marbl/canu)
   - [Flye](https://github.com/fenderglass/Flye)
   - [Benchmark](https://f1000research.com/articles/8-2138)

 - Genome annotation :

# Genomic

This  would involve (at least) 5 parts:
 - QC (Quality Control) : 
   - [NanoComp](https://github.com/wdecoster/NanoComp) 
   - [pycoQC](https://github.com/a-slide/pycoQC)
 - Mapping
   - [Minimap2](https://github.com/lh3/minimap2)
   - [NGMLR](https://github.com/philres/ngmlr) for SVs


# FASTQ manipulation
 - [Samtools](https://github.com/samtools/samtools)
 - [Nanotripper](https://github.com/nodrogluap/nanostripper)
