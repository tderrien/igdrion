## Copy on Nanoserver (not tested)

To be done from `Genouest` to mounted `Nanoserver`.

`rsync -avh tderrien@genossh.genouest.org:/groups/dnarxiv/data/20220118_dnarXiv_Flongle_Julien4/ /Volumes/Nanopore/`
```
NANOSERVER_IP="129.20.155.230"
PREFIX_NANOSERVER_PATH="/volume1/Nanopore"

function launch_rsync {
	hostname=$(echo "${2}" | cut -d@ -f2)
	echo -e "\033[0;34m*************************************************"
	echo -e "Starting copy to ${hostname}...\n\033[0m"

	# archive, verbose, copy symlinks, resume if error
	rsync \
		-avLP \
		--perms --chmod=D2775,F664 \
		--ignore-existing \
		"$1" \
		"${2}:${3}"

	# Check last command exit code
	if [ $? -eq 0 ]; then
		echo -e "\n\033[0;32mCopy to ${hostname} successfully done\033[0;31m"
		echo -e "\033[0;34m*************************************************"
	else
		echo -e "\n\033[0;31mCopy to ${hostname} failed\033[0;31m"
		echo -e "\033[0;34m*************************************************"
	fi
}

launch_rsync "$1/" "${USER}@${NANOSERVER_IP}" "${PREFIX_NANOSERVER_PATH}/$(basename "${1}")"

```

## To connect to mk1C

```
ssh minit@mc-111744.local
# data are in directory
cd /data/
# to copy data on genouest
rsync -ravh --relative --size-only --no-perms --no-owner --no-group  --itemize-changes --progress --stats \
./20210715_dnarXiv_Flongle_Julien1/ \
tderrien@genossh.genouest.org:/groups/dnarxiv/data/

# to copy data on RackStation
## WARNINGS: it creates a /data in the target directory
rsync -ravh --relative --size-only --no-perms --no-owner --no-group  --itemize-changes --progress --stats minit@mc-111744.local:/data/20210727_JULIEN2/ /Volumes/Chiens/NanoporeDATA/

```

## Procédure to connect to minIT and airminion 

Copy one directory called `20210715_dnarXiv_Flongle_Julien1/` from **MacBook-air** to the genocluster directory `/groups/dnarxiv/data/`:

```
rsync -ravh --relative --size-only --no-perms --no-owner --no-group  --itemize-changes --progress --stats \
/Library/MinKNOW/data/20210715_dnarXiv_Flongle_Julien1/ \
tderrien@genossh.genouest.org:/groups/dnarxiv/data/
```

Easiest way for 2 directories `2021030*` to be copied from **minit** to genouest
```
# connect to minit and go to data
ssh minit@129.20.174.103
cd /data/

# rsync with genossh
rsync -ravh --relative --size-only --no-perms --no-owner --no-group  --itemize-changes --progress --stats \
./2021030*/  tderrien@genossh.genouest.org:/groups/dog/tderrien/nanopore/dog/


```



 ### - Cas n°1 : **sans ssh** (deprecated => Fe2021 ; New software release : Minion software)
 
 Le minIT (**MT-111149**) est connecté par éthernet et le Mac (**AirMinion**) n'est pas connecté par ethernet ni par wifi.
   1. Brancher le minIT.
   2. Se connecter au  minIT via le wifi (pwd : WarmBut......Wings98)) 
   3. Accéder au minIT via le Finder (réseau partagé) MT-111149 (pwd: minit (double))
   4. Se connecter à RackstationIGDR via login/pwd perso (nécessite de connecter l'ordi au réseau avec cable ethernet)
   5. Copier les données du minIT à Rackstation (cf section #Rsync)

 ### - Cas n°2 : **avec ssh**

 MinIT (**MT-111149**) est connecté par éthernet et le Mac (**AirMinion**) n'est pas connecté par ethernet.
```
ssh minit@MT-111149.local
# or
ssh minit@129.20.174.103 # a retester

```



### Rsync = Copy de données de séquençage

For copying/sync'ing data between 2 repositories (minit et Rackstation):

 - Vérifier que **AirMinion** est connecté à RackstationIGDR (sur le volume Chiens) : `ls /Volumes/Chiens/NanoporeDATA/`
 - Si le répertoire/run séquencé s'appelle `20210222`. Il sera sur le **minIT** ici :  `/data/2021022`
 - Pour le copier sur RackStation:
```
rsync -ravh  --size-only --no-perms --no-owner --no-group  --itemize-changes --progress --stats minit@129.20.174.103:/data/2021022/ /Volumes/Chiens/NanoporeDATA/
# cat output_rsync3.txt  | grep -v ".f..t......"
```
 - si la copie est longue, il est possible d'utiliser : `caffeinate -di` (pour inactiver la veille automatique)


With options :
 -r, --recursive             recurse into directories   
 -a , --archive               archive mode; equals -rlptgoD (no -H,-A,-X)   
 -v, --verbose               increase verbosity   
 **-n, --dry-run               perform a trial run with no changes made**   
 -h, --human-readable        output numbers in a human-readable format   
 --size-only             skip files that match in size  
 -i, --itemize-changes       output a change-summary for all updates   

Example real copying
```
rsync --ignore-existing --recursive --human-readable --progress  -t minit@129.20.174.103:/data/20200721/ .
```
